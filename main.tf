
terraform {
  required_version = ">= 1.0.0"

  backend "s3" {
    bucket         = "kojitechs.3.tier.arch.jnjames"
    # dynamodb_table = "terraform-state-lock1"
    region         = "us-east-1"
    key            = "path/env"
    encrypt        = true

  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region  = "us-east-1"
  profile = "default"
}

locals {
  vpc_id = aws_vpc.kojitechs_vpc.id
  azs    = data.aws_availability_zones.available.names
}

data "aws_availability_zones" "available" {
  state = "available"

}

#Creating VPC
resource "aws_vpc" "kojitechs_vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "kojitechs_vpc"
  }
}

#Creating internet gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = local.vpc_id

  tags = {
    Name = "igw"
  }

}

#Creating Public Subnets
resource "aws_subnet" "public_subnet" {
  count = length(var.public_cidr) #telling terraform to calculate the size of the public cidr variable

  vpc_id                  = local.vpc_id
  cidr_block              = var.public_cidr[count.index]
  availability_zone       = element(slice(local.azs, 0, 2), count.index)
  map_public_ip_on_launch = true

  tags = {
    Name = "public_subnet_${count.index + 1}"
  }

}

#Creating Private Subnets
resource "aws_subnet" "private_subnet" {
  count = length(var.private_cidr) #telling terraform to calculate the size of the private cidr variable

  vpc_id            = local.vpc_id
  cidr_block        = var.private_cidr[count.index]
  availability_zone = element(slice(local.azs, 0, 2), count.index)

  tags = {
    Name = "private_subnet_${count.index + 1}"
  }

}

#Creating Database subnet
resource "aws_subnet" "database_subnet" {
  count = length(var.database_cidr) #telling terraform to calculate the size of the private cidr variable

  vpc_id            = local.vpc_id
  cidr_block        = var.database_cidr[count.index]
  availability_zone = element(slice(local.azs, 0, 2), count.index)

  tags = {
    Name = "database_subnet_${count.index + 1}"
  }

}





























































