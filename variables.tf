
variable "vpc_cidr" {
  description = "cidr block for the vpc"
  type        = string
  default     = "10.0.0.0/16"
}

variable "public_cidr" {
  description = "cidr block for the public subnet"
  type        = list(any)
  default     = ["10.0.0.0/24", "10.0.2.0/24"]
}

variable "private_cidr" {
  description = "cidr block for the private subnet"
  type        = list(any)
  default     = ["10.0.1.0/24", "10.0.3.0/24"]
}

variable "database_cidr" {
  description = "cidr block for the database subnet"
  type        = list(any)
  default     = ["10.0.51.0/24", "10.0.53.0/24"]
}


# variable "public_subnet" {
#     description = "public subnet"
#     type = string
# }


# variable "private_subnet" {
#     description = "private subnet"
#     type = string
# }



# variable "database_subnet" {
#     description = "database subnet"
#     type = string
# }

